import React from 'react';

const path = '/500';
const action = () => <ServerError />;

function ServerError() {
  return (
    <div>500 Server Error occured</div>
  );
}

export default { path, action };