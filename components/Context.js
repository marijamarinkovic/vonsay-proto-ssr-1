import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Context extends Component {
  static childContextTypes = {
    user: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
    page: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      status: PropTypes.number
    })
  }

  getChildContext() {
    return {
      user: this.props.user,
      page: this.props.page
    }
  }

  render() {
    return React.Children.only(this.props.children);
  }

}

export default Context;