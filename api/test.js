import express from 'express';

const router = express.Router();

router.use((req, res, next) => next());

router.get('/test', (req, res) => {
  res.send({message: 'Hello from RESTful API'});
});

export default router;
