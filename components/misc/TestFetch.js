import React, { Component } from 'react';
import fetch from '../../core/fetch';

class TestFetch extends Component {
  state = {
    data: 'loading...'
  }

  async componentDidMount() {
    try {
      // const response = await fetch('https://jsonplaceholder.typicode.com/posts/1');

      const response = await fetch('/api/test');
      const data = await response.json();

      this.setState({data: JSON.stringify(data)});
    } catch(e) {
      this.setState({data: 'Error ' + err.message});
    }
  }

  render() {
    return <p>Server response: {this.state.data}</p>
  }
}

export default TestFetch;