import { GraphQLString as StringType } from 'graphql';

const greeting = {
  type: StringType,
  description: 'Generates a welcome message',
  args: {
    name: { type: StringType, description: 'First name' }
  },
  resolve(_, { name }) {
    // return `Welcome, ${name || 'Guest'}!`;
    return new Promise(resolve => setTimeout(resolve.bind(`Welcome, ${name || 'Guest'}!`, 3000)));
  }
};

export default greeting;