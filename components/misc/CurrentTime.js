import React from 'react';
import { canUseDOM } from 'fbjs/lib/ExecutionEnvironment';

function CurrentTime() {
  const elem = canUseDOM && document.querySelector('.time[data-time]');
  const time = elem ? +elem.dataset.time : Date.now();

  return (
    <p className="time" data-time={time}>Current time: { time }</p>
  );
}

export default CurrentTime;