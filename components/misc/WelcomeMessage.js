import React, { Component } from 'react';
import PropTypes from 'prop-types';

class WelcomeMessage extends Component {
  static contextTypes = {
    user: PropTypes.shape({
      name: PropTypes.string.isRequired
    })
  }

  render() {
    const user = this.context.user;
    return <p>Welcome, { user ? user.name : 'Guest'}!</p>;
  }
}

export default WelcomeMessage;