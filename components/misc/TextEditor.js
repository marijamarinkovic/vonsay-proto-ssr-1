import React, { Component } from 'react';

class TextEditor extends Component {
  componentDidMount() {
    this.codeMirror = require('codemirror')
      .fromTextArea(this.inputRef, {
        value: this.props.children || 'default value',
        mode: 'javascript'
      });
    this.codeMirror.on('change', this.handleChange);
  }

  componentWillUnmount() {
    if (this.codeMirror) {
      this.codeMirror.toTextArea();
    }
  }

  handleChange = editor => {
    if (this.props.onChange) {
      this.props.onChange(editor.getValue());
    }
  }

  render() {
    return <textarea ref={el => this.inputRef = el} />
  }
}

export default TextEditor;