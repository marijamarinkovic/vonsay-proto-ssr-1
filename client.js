import 'babel-core/register';

import React from 'react';
import ReactDOM from 'react-dom';
import Router from './core/Router';

function run() {
  const [component, page] = Router.match({
    path: window.location.pathname
  }, window.AppState);
  ReactDOM.hydrate(
    component,
    document.getElementById('app'),
    () => {
      document.title = page.title;
      document.querySelector('meta[name=description]')
        .setAttribute('content', page.description);
    }
  );
}

const loadedStates = ['complete', 'loaded', 'interactive'];

if (loadedStates.includes(document.readyState) && document.body) {
  run();
} else {
  window.addEventListener('DOMContentLoaded', run, false);
}