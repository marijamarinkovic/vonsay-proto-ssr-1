import React from 'react';
import Context from '../components/Context';

const routes = [
  require('../routes/Home').default,
  require('../routes/NotFound').default,
  require('../routes/ServerError').default
];

const router = {
  match(location, state) {
    let component;
    const page = {
      title: 'My app',
      description: 'Iso web app',
      status: 200
    };
    const route = routes.find(x => x.path === location.path);

    if (route) {
      try {
        component = route.action(location);
      } catch (err) {
        component = routes.find(x => x.path === '/500').action();
        page.status = 500;
      }
    } else {
      component = routes.find(x => x.path === '/404').action();
      page.status = 404;
    }

  return [<Context page={page} {...state}>{component}</Context>, page];
  }
};

export default router;