import React from 'react';
import WelcomeMessage from '../../components/misc/WelcomeMessage';

function Hero() {
  return (
    <div>
      <h2>Rent Anything You Want</h2>
      <p>From people around you</p>
      <form>
        <input
          type="search"
          placeholder="I want to rent..." />
        <button>Search</button>
      </form>

      <hr />
      <WelcomeMessage />
    </div>
  );
}

export default Hero;